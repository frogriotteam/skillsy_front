<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>
        <section class="register">
            <div class="register__side">
                <div class="aaa">
                    <img src="assets/img/img-rejestracja.jpg" alt="">
                    <div class="content">
                        <div id="opinions-slider">
                            <div class="item">
                                <div class="main">
                                    Dzięki Skillsy znalazłam platformę, która szuka uczniów za mnie - oszczędzony czas poświęcam na naukę!
                                </div>
                                <div class="note">
                                    AGNIESZKA ZIELIŃSKA, POLONISTKA
                                </div>
                            </div>
                            <div class="item">
                                <div class="main">
                                    Dzięki Skillsy znalazłam platformę, która szuka uczniów za mnie - oszczędzony czas poświęcam na naukę!
                                </div>
                                <div class="note">
                                    AGNIESZKA ZIELIŃSKA, POLONISTKA
                                </div>
                            </div>
                            <div class="item">
                                <div class="main">
                                    Dzięki Skillsy znalazłam platformę, która szuka uczniów za mnie - oszczędzony czas poświęcam na naukę!
                                </div>
                                <div class="note">
                                    AGNIESZKA ZIELIŃSKA, POLONISTKA
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="register__main content__wrapper">
                <header class="section-header section__header__level__1">
                    <h1 class="header__level_1">Rejestracja nowego konta</h1>
                    <div class="header__note">Załóż darmowe konto - zdobywaj i dziel się umiejętnościami</div>
                </header>  
                <form id="register-form" class="form">
                    <ul class="fields">
                        <div class="form-note form-note--mobile form__row required">
                            <div class="label-box">
                                Pola wymagane
                            </div>    
                        </div>
                        <li class="form__row required">
                            <div class="label-box">E-mail</div>
                            <div class="field-box">
                                <input type="email" name="email" id="email">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                        <li class="form__row required">
                            <div class="label-box">Nazwa użytkownika</div>
                            <div class="field-box">
                                <input type="text" name="user_name" id="user_name">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                        <li class="form__row required error">
                            <div class="label-box">Imię</div>
                            <div class="field-box">
                                <input type="text" name="name" id="name">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                        <li class="form__row required">
                            <div class="label-box">Nazwisko</div>
                            <div class="field-box">
                                <input type="text" name="surname" id="surname">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                        <li class="form__row">
                            <div class="label-box">Nazwa firmy</div>
                            <div class="field-box">
                                <input type="text" name="company_name" id="company_name">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                        <li class="form__row required">
                            <div class="label-box">Hasło</div>
                            <div class="field-box">
                                <input type="password" name="pass" id="pass">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                        <li class="form__row required">
                            <div class="label-box">Powtórz hasło</div>
                            <div class="field-box">
                                <input type="password" name="repeat_pass" id="repeat_pass">
                                <span class="error-txt">Pole wymagane</span>
                            </div>
                        </li> 
                    </ul>
                    <label class="checkbox-ctn">
                        <div class="field-box">
                            <input type="checkbox">
                            <span class="state"></span>
                        </div>
                        <div class="txt-box">
                            Akceptuję <a href="#">regulamin</a> serwisu
                        </div>
                    </label>
                    <label class="checkbox-ctn">
                        <div class="field-box">
                            <input type="checkbox">
                            <span class="state"></span>
                        </div>
                        <div class="txt-box">
                            Wyrażam zgodę na otrzymywanie newslettera i informacji handlowych od Skillsy
                        </div>
                    </label>
                    <button type="submit" class="btn btn-wide">Założ konto</button>
                    <div class="form-note form-note--desktop form__row required">
                        <div class="label-box">
                            Pola wymagane
                        </div>    
                    </div>
                </form>
            </div>
        </section>
        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
