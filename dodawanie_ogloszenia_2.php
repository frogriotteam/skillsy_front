<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>
        <div class="content__wrapper">
            
            <section class="new__announce">
                <header class="section-header">
                    <h1 class="header__level__1 center">
                        Dodawanie nowego ogłoszenia
                    </h1>
                </header>
                <div class="new__anounce__container">
                    <div class="content__container__desktop">
                            <div class="steps">
                                <button class="step-1 step active">
                                    <span class="num">1</span><span class="txt">01. Dane szkolenia</span>
                                </button>
                                <button class="step-arrow step-1-arrow active"></button>
                                <button class="step-2 step active active-step">
                                    <span class="num">2</span><span class="txt">02. Promocje</span>
                                </button>
                                <button class="step-arrow step-2-arrow active"></button>
                                <button class="step-3 step">
                                    <span class="num">3</span><span class="txt">03. Potwierdzenie</span>
                                </button>
                            </div>
                            <div class="new__announce__content">
                                <form id="register-form" class="form">
                                    <div class="register__step register__step--step_1">
                                        <div class="new__announce__part">
                                            <h2>Informacje podstawowe</h2>

                                            <ul class="fields">
                                                <li class="form__row required">
                                                    <div class="label-box required">Nazwa ogłoszenia</div>
                                                    <div class="field-box">
                                                        <input type="text" name="name" id="name">
                                                    </div>
                                                </li>
                                                <li class="form__row required">
                                                    <div class="label-box required error">Kategoria</div>
                                                    <div class="field-box">
                                                        <select class="select" name="kategoria" id="category">
                                                            <option>kategoria 1</option>
                                                            <option>kategoria 2</option>
                                                            <option>kategoria 3</option>
                                                        </select>
                                                    </div>
                                                </li>
                                                <li class="form__row required">
                                                    <div class="label-box required error">Opis</div>
                                                    <div class="field-box">
                                                        <textarea rows="6" name="description" id="description"></textarea>
                                                    </div>
                                                </li>
                                                <li class="form__row required">
                                                    <div class="label-box required">Zdjęcie</div>
                                                    <div class="field-box">
                                                        <input type="file" name="zdjecie" id="photo" class="jfilestyle" data-text="Dodaj zdjęcie" data-buttonBefore="true">
                                                    </div>
                                                </li>
                                            </ul>

                                            <div class="info-box">
                                                <span class="icon icon-icon-info"></span>
                                                <div class="txt">Korzystaj z legalnych zdjęć, najlepiej darmowych lub swojego autorstwa i pamiętaj o ich dobrej jakości!</div>
                                            </div>
                                        </div>
                                        <div class="new__announce__part">
                                            <h2>Cena</h2>
                                            <ul class="fields">
                                                <li class="form__row required">
                                                    <div class="label-box required">Cena w PLN</div>
                                                    <div class="field-box">
                                                        <input type="text" name="cena" id="cena">
                                                    </div>
                                                </li>
                                            </ul>   
                                            <div class="info-box">
                                                <span class="icon icon-icon-info"></span>
                                                <div class="txt">Wypunktuj lub opisz co obejmuje cena (możesz skorzystać z obydwóch sposobów).</div>
                                            </div>
                                            <ul class="fields">
                                                <li class="form__row form__row__point">
                                                    <div class="label-box required">1.</div>
                                                    <div class="field-box">
                                                        <input type="text" name="cena" id="cena">
                                                    </div>
                                                    <div class="remove-box">x</div>
                                                </li>
                                                <li class="form__row form__row__point">
                                                    <div class="label-box required">2.</div>
                                                    <div class="field-box">
                                                        <input type="text" name="cena" id="cena">
                                                    </div>
                                                    <div class="remove-box">x</div>
                                                </li>
                                                <li class="form__row form__row__point">
                                                    <div class="label-box required">3.</div>
                                                    <div class="field-box">
                                                        <input type="text" name="cena" id="cena">
                                                    </div>
                                                    <div class="remove-box">x</div>
                                                </li>
                                                <li class="form__row form__row__point">
                                                    <div class="label-box"></div>
                                                    <div class="field-box">
                                                        <button id="add__option" type="button" class="form__btn">Dodaj opcję</button>
                                                    </div>
                                                </li>
                                            </ul>   
                                        </div>
                                        <div class="new__announce__part">
                                            <h2>Cena</h2>
                                            <ul class="fields">
                                                <li class="form__row required">
                                                    <div class="label-box required">Opis</div>
                                                    <div class="field-box">
                                                        <textarea name="price_description" rows="6"></textarea>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>  
                                        <div class="new__announce__part">
                                            <h2>Lokalizacja</h2>
                                            <div class="info-box">
                                                <span class="icon icon-icon-info"></span>
                                                <div class="txt">Wprowadź miasto oraz wszelkie inne szczegóły dotyczące lokalizacji (adres, piętro itp.)</div>
                                            </div>
                                            <ul class="fields">
                                                <li class="form__row required">
                                                    <div class="label-box required">Miasto</div>
                                                    <div class="field-box">
                                                        <input type="text" name="city" id="city">
                                                    </div>
                                                </li>
                                                <li class="form__row">
                                                    <div class="label-box">Opis</div>
                                                    <div class="field-box">
                                                        <textarea name="city_description" rows="6"></textarea>
                                                    </div>
                                                </li>
                                            </ul>    
                                        </div> 
                                        <div class="new__announce__part">
                                            <h2>Termin</h2>
                                            <div class="info-box">
                                                <span class="icon icon-icon-info"></span>
                                                <div class="txt">Wprowadź konkretny termin lub jeżeli szkolenie nie ma konkretnego terminu opisz pod jakimi warunkami szkolenie się odbędzie.</div>
                                            </div>
                                            <div class="form-radio">
                                                <div class="row">
                                                    <div class="col col-6">
                                                        <label class="radio-ctn">
                                                            <input type="radio" name="term" checked>
                                                            <div class="state">
                                                                <span class="control"></span>
                                                                <span class="txt">Mam konkretny termin</span>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col col-6">
                                                        <label class="radio-ctn">
                                                            <input type="radio" name="term">
                                                            <div class="state">
                                                                <span class="control"></span>
                                                                <span class="txt">Nie mam terminu</span>
                                                            </div>        
                                                        </label>
                                                    </div>
                                                </div>
                                                <ul class="fields">
                                                    <li class="form__row required">
                                                        <div class="label-box required">Wybierz date</div>
                                                        <div class="field-box">
                                                            <input type="text" name="date" id="date">
                                                        </div>
                                                    </li>
                                                    <li class="form__row required">
                                                        <div class="label-box">Wybierz godzinę</div>
                                                        <div class="field-box">
                                                            <input type="text" name="hour">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 
                                        <div class="new__announce__part">
                                            <h2>Umiejętności</h2>
                                            <div class="info-box">
                                                <span class="icon icon-icon-info"></span>
                                                <div class="txt">Wpisz umiejętności jaki zdobędzie szkolący się</div>
                                            </div>
                                            <input type="text" data-role="tagsinput" />
                                            <div class="center">
                                                <a href="#" class="btn" id="next__step">Dalej</a>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="register__step register__step--active register__step--step_2">
                                        <div class="register__features">
                                            <a href="#" class="feature-box">
                                                <img src="assets/img/star_round.svg" alt="" class="icon">
                                                <h2>Wyróżnij graficznie</h2>
                                                <div class="price__box">
                                                    <div class="price">4,90</div>
                                                    <div class="note">zł</div>
                                                </div>
                                                <ul class="list">
                                                    <li>Aliquet diam gravida</li>
                                                    <li>Phasellus eu condimentum</li>
                                                    <li>Metus non venenatis turpis</li>
                                                </ul>
                                                <div class="center">
                                                    <span class="btn">Wybieram</span>
                                                </div>
                                            </a>
                                            <a href="#" class="feature-box">
                                                <img src="assets/img/star_round.svg" alt="" class="icon">
                                                <h2>Przesuń na szczyt listy wyszukiwań</h2>
                                                <div class="price__box">
                                                    <div class="price">4,90</div>
                                                    <div class="note">zł</div>
                                                </div>
                                                <ul class="list">
                                                    <li>Aliquet diam gravida</li>
                                                    <li>Phasellus eu condimentum</li>
                                                    <li>Metus non venenatis turpis</li>
                                                </ul>
                                                <div class="center">
                                                    <span class="btn">Wybieram</span>
                                                </div>
                                            </a>
                                            <a href="#" class="feature-box">
                                                <img src="assets/img/star_round.svg" alt="" class="icon">
                                                <h2>Przesuń na szczyt listy wyszukiwań</h2>
                                                <div class="price__box">
                                                    <div class="price">4,90</div>
                                                    <div class="note">zł</div>
                                                </div>
                                                <ul class="list">
                                                    <li>Aliquet diam gravida</li>
                                                    <li>Phasellus eu condimentum</li>
                                                    <li>Metus non venenatis turpis</li>
                                                </ul>
                                                <div class="center">
                                                    <span class="btn">Wybieram</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="center">
                                            <a href="#" class="btn" id="next__step">Pomiń</a>
                                        </div>
                                    </div>
                                </form> 
                                    
                            </div>
                        </div>
                </div>
            </section>
            
        </div>
        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
