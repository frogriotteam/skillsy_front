<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="profile profile__add-opinion">
            <div class="profile__head">
                <div class="container">
                    <div class="profile__head__inner">
                        <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="profile__profile">
                                <div class="img-box">
                                    <img src="assets/img/avatar_3.png" alt="">
                                </div>
                                <div class="content-box">
                                    <h1>Magda Markowska</h1>
                                    <div class="location"><span class="icon icon-pin"></span>Warszawa</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="profile__ranking">
                                <div class="num-box">4.5</div>
                                <div class="content-box">
                                    <div class="stars">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star0.5.svg">
                                    </div>
                                    <span class="txt">na podstawie</span>
                                    <a href="#" class="opinions">15 opinii</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="profil__actions">
                                <a href="#" class="btn">Kontakt</a>
                                <a href="#" class="btn btn--white"><span class="icon icon-alarm-comments"></span><span class="txt">Wiadomość</span></a>
                            </div>
                        </div>
                    </div>
                    </div>    
                </div>
            </div>
            
            <div class="content__wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-6 offset-lg-3">
                            <h2 class="header__level__2">Dodaj opinię</h2>
                            <div class="content__container__desktop">
                                <form class="add_opinion form" action="#">
                                    <div class="part">
                                        <div class="header">Ocena:</div>
                                        <div class="raty-box">
                                            <div id="raty"></div>
                                            <div id="target"></div>
                                        </div>    
                                    </div>
                                    <div class="part">
                                        <div class="header">Treść:</div>
                                        <textarea rows="6" name="description"></textarea>
                                    </div>
                                    <div class="part">
                                        <button type="submit" class="btn">Dodaj opinię</button>
                                    </div>
                                </form>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
