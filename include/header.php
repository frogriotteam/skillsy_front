<nav id="nav" class="nav">
    <div class="nav__side nav__side--left">
        <a href="/" class="logo"><img src="assets/img/logo.svg" alt=""></a>
        <select class="chosen-select" data-no_results_text="Brak wyników">
            <option>Odkryj kursy</option>
            <option>Matematyka</option>
            <option>Historia</option>
            <option>Biologia</option>
        </select>  
    </div>
    <div class="nav__side nav__side--right">
        <a href="#" class="notify">
            <img src="assets/img/alarm-bell.svg" alt="">
            <span class="notify__num">2</span>
        </a>
        <a href="#" class="notify">
            <img src="assets/img/alarm-comments.svg" alt="">
            <span class="notify__num">3</span>
        </a>
        <a href="#" class="avatar">
            <img src="assets/img/avatar.jpg" alt="">
        </a>
    </div>
    <a href="#" class="btn-login">Zaloguj</a>
</nav>