const scrollToFixed = () => {
    
    require('scrolltofixed');

    var stf = $('.scrolltofixed');

    if ( $(window).width() < 1200) {
        
        stf.trigger('detach.ScrollToFixed');

    }else{
        stf.scrollToFixed({
            limit: function(){
    
                var offset = $('.page-content').height(),
                    limit = Math.round(offset);
                    
                return limit;
    
            },
            unfixed: function(){
                $(this).css({
                    'right':0,
                    'top':0,
                    'top':'inherit'
                });
            }
        });
    }

}

module.exports = {
    scrollToFixed,
};

