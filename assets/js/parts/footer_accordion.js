const footerAccordion = () => {
    
    if($(window).width() < 992) {
    
        var acc = $('.footer-links .header')
        var i;
        
        $('.footer-links .col:first-child').find('.header').addClass('active')
        
        $('.footer-links .col').not(":first-child").find('.list').css('display','none')

        acc.each(function(){

            $(this).click(function(){
                
                if ($(this).hasClass('active')) {
                    return;
                }
                
                $('.footer-links .col').find('.header').removeClass('active')
                
                $('.footer-links').find('.list').slideUp()
                
                $(this).addClass('active')
                $(this).parent().find('.list').slideDown()
            })
        })
    }
    
 }



 module.exports = {
     footerAccordion
 };