<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>
        <div class="content__wrapper">
            
            <section class="new__announce">
                <div class="new__anounce__container">
                    <div class="content__container__desktop mobile__shadow__box">
                            <div class="steps steps-mobile-none">
                                <button class="step-1 step active active-step" id="step-1">
                                    <span class="num">1</span><span class="txt">01. Dane szkolenia</span>
                                </button>
                                <button class="step-arrow step-1-arrow active"></button>
                                <button class="step-2 step" id="step-2">
                                    <span class="num">2</span><span class="txt">02. Promocje</span>
                                </button>
                                <button class="step-arrow step-2-arrow"></button>
                                <button class="step-3 step">
                                    <span class="num">3</span><span class="txt">03. Potwierdzenie</span>
                                </button>
                            </div>
                            <div class="new__announce__content">
                                <form id="register-form" class="form"> 
                                    <div class="register__step register__step--active step__end register__step--step_3">
                                        <div class="icon-finish">
                                            <img src="assets/img/check_2.svg" alt="">
                                        </div>
                                        <h2 class="header__level__2">Gotowe</h2>
                                        <div class="desc">
                                            Twoje ogłoszenie zostało dodane. W ciagu kilku minut ogłoszenie pojawi się na stronie.
                                        </div>
                                        <div class="center">
                                            <a href="#" class="btn">Wróć do strony głównej</a>
                                        </div>    
                                    </div>   
                                </form> 
                                    
                            </div>
                        </div>
                </div>            
            </section>
            
        </div>
        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
