<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="blue__header">
            <img src="assets/img/music2.png">
            <div class="content">
                <div class="container">
                    <h1>Regulamin</h1>    
                </div>
            </div>    
        </div>
        
        <div class="content__color">
            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="#">Skillsy</a></li>
                    <li><a href="#">Page</a></li>
                    <li>Regulamin</li>
                </ul>
                <h2 class="header__level__2 header__align__left">Regulamin portaly Skillsy</h2> 
                <p>    
                    Aliquam mi purus, ornare sit amet blandit at, ornare volutpat dui. In et sem dolor. Pellentesque accumsan mauris quis lacus suscipit egestas. Quisque quis viverra sem. Nam in nisi ut mauris maximus dapibus. Quisque elit ex, semper sit amet nisi at, sodales convallis orci. Vestibulum sodales felis at vestibulum efficitur. Praesent eu augue magna. Nam dictum eget ligula eget condimentum. Vivamus luctus rutrum lorem id porttitor. Mauris viverra imperdiet neque, vel finibus est. Duis sit amet elementum dui. Vivamus lectus purus, laoreet et venenatis ut, placerat nec nunc. Donec a nunc id sem mollis consectetur. Nunc felis arcu, malesuada vel libero et, faucibus condimentum metus. Vivamus sed leo ligula.
                </p>
                <p>    
                    Vestibulum iaculis imperdiet ex, ut tristique lacus aliquam vitae. Morbi condimentum rhoncus justo, non lobortis turpis volutpat quis. Integer vulputate felis quis mi semper dictum id at felis. Vivamus egestas lorem et nulla luctus placerat. Aliquam sit amet augue hendrerit, dapibus mi ac, rhoncus nunc. Proin turpis lectus, commodo at blandit sit amet, ultrices at tellus. Maecenas condimentum, erat vel cursus suscipit, lorem metus maximus libero, sed condimentum libero dui ac ex. Pellentesque accumsan efficitur ligula, convallis fringilla ante mollis et. Vivamus ut mi tincidunt, blandit massa ut, accumsan elit. Proin convallis non justo interdum efficitur. Donec rhoncus vitae justo volutpat tempus. Donec vitae turpis ante. In congue, nisi in condimentum mattis, augue nunc cursus augue, at lacinia magna libero sit amet justo.
                </p>
                <p>    
                    Morbi vel mauris eu libero hendrerit fringilla ut at nunc. Pellentesque efficitur dignissim turpis in efficitur. Donec ornare lorem vel ex facilisis fermentum. Nulla rhoncus tortor id malesuada sodales. Duis vel massa at quam tempor gravida commodo at lorem. Nullam sit amet sodales arcu. In lorem tortor, placerat sit amet condimentum non, porttitor in lectus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur quis sapien nibh. Nullam orci ipsum, dignissim quis vehicula a, condimentum id tellus. In ut quam quis dui consectetur tristique. Sed vitae velit id lacus cursus interdum quis non felis.
                </p>
                <p>    
                    Pellentesque sollicitudin scelerisque sapien ut tristique. Quisque cursus ultricies sagittis. Donec fringilla, orci non volutpat auctor, ante purus laoreet eros, at tempor orci ligula quis arcu. Aenean vestibulum maximus sapien, ut tristique ante mollis eu. Nunc efficitur, libero quis pellentesque interdum, dolor quam viverra tellus, non bibendum eros leo non eros. Pellentesque lobortis rhoncus libero, sed lacinia felis porta nec. Nam ut vestibulum lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet ornare odio. Integer pellentesque iaculis erat vel viverra. Quisque dignissim tincidunt consectetur. Phasellus mauris tortor, posuere vel nulla eget, aliquam hendrerit nisi. Quisque commodo nisl pretium porttitor vehicula.
                </p>
                <p>    
                    Donec euismod non sapien a tempor. Aliquam nunc turpis, porta ac tempus sit amet, posuere id turpis. Mauris ac erat felis. Sed erat arcu, sollicitudin et enim at, suscipit mattis justo. Phasellus vitae ultrices risus, at imperdiet tellus. Aliquam sed risus elementum, elementum tortor at, fringilla neque. Mauris finibus ante sit amet nunc euismod, in consequat ipsum vestibulum. Proin eget odio rhoncus nibh tempus luctus. Nunc lacus nunc, placerat eget mollis dignissim, accumsan et nisi. Quisque faucibus mi vitae pellentesque dictum. Etiam a dignissim sem. Etiam finibus blandit maximus. Vivamus mattis elementum purus, vel sodales ipsum efficitur ut. Curabitur hendrerit erat ac libero sagittis euismod. Pellentesque cursus dolor mi, venenatis eleifend nibh gravida vitae.
                </p>
                <p>    
                    Proin convallis et ligula sit amet cursus. In tincidunt imperdiet vehicula. Vestibulum fringilla metus lobortis, tincidunt dolor non, lacinia arcu. Donec tempor dui sit amet velit volutpat, quis sagittis ex ultricies. Etiam at massa malesuada, tincidunt sapien eget, rhoncus ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent sit amet nibh nec nisl pellentesque ultricies. Integer posuere suscipit tincidunt. Fusce et diam nibh.
                </p>    
                <p>    
                    Aliquam sit amet urna a neque bibendum sollicitudin vitae at ipsum. Morbi dictum orci eget diam consequat, ut condimentum augue condimentum. Cras ac convallis est, ac sollicitudin odio. Donec luctus felis quam. Sed ornare vel ante dignissim condimentum. Vestibulum augue lacus, elementum nec odio sed, feugiat suscipit augue. Maecenas dolor quam, rutrum rhoncus tellus at, fringilla sagittis dui. Curabitur ut est lacinia, sagittis lectus ut, auctor tortor. Duis consequat lectus at finibus fermentum. Etiam maximus ullamcorper nulla suscipit volutpat. Aenean efficitur, sem molestie molestie euismod, felis eros efficitur massa, non tristique augue lectus in nisl. Nunc auctor turpis ut justo vehicula accumsan.
                </p>    
            </div>    
        </div>
        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
