<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="row">
                <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                    <div class="content__container desktop__container">
                        <section class="reset-password">
                            <img src="assets/img/envelope.svg" alt="" class="icon">
                            <header class="section-header">
                                <h2 class="header__level__2">Resetowanie hasła</h2>
                                <div class="desc">
                                    Jeżeli podałeś poprawny adres e-mail, otrzymasz na niego wiadomość z instrukcjami dotyczącymi resetu hasła.
                                </div>
                            </header>

                            <a href="#" class="btn btn-wide submit">Przejdź do strony logowania</a>
                            
                        </section>
                    </div>
                </div>
            </div>

        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
