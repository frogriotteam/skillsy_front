const slidingMenu = () => {

    var slideMenu = $('.panel-nav__list'),
        slideMenu_slider = $('#panel-nav-slider'),
        slideMenu_slider_offset = 13,
        top;


    if (slideMenu.length > 0) {

        var slideMenu_a = slideMenu.find('li > a');

        if (slideMenu_a.hasClass('active')) {

            top = $('.panel-nav__list > li a.active').position().top - slideMenu_slider_offset;

            slideMenu_slider.css({
                'top': top
            });

        } else {

            top = -400;

            slideMenu_slider.css({
                'top': top,
            });

        }

        slideMenu_a.hover(function () {

            top = $(this).position().top - slideMenu_slider_offset;

            slideMenu_slider.stop().animate({
                'top': top,
            }, 300, 'swing');

        }, function () {

            if (slideMenu_a.hasClass('active')) {
                
                top = $('.panel-nav__list > li a.active').position().top - slideMenu_slider_offset;

            } else {
                top = -400;
            }

            slideMenu_slider.stop().animate({
                'top': top,
            });

        });

    }

}

module.exports = {
    slidingMenu,
};