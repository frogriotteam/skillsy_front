<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <nav class="course-nav d-flex d-lg-none">
            <button data-menu="course-menu" class="hamburger hamburger--3dxy" type="button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </nav>

        <div class="body-wrapper body-wrapper--gray">

            <div class="banner">
                <div class="banner__img">
                    <img src="assets/img/music2.png" />
                    <div class="container d-block d-lg-none relative">
                        <div class="banner__cat">MUZYKA</div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-lg-8 lg-static">
                            <div class="banner__content">
                                <h1 class="banner__header">Kurs gry na gitarze - kurs podstawowy<br/> dla dzieci do 18 lat</h1>
                                <div class="lclz">
                                    <div class="lclz__header"><i class="icon-location"></i>Warszawa</div>
                                </div>
                            </div>
                        </div>
                        <div class="d-none d-lg-flex col-sm-4">
                            <div class="rating-box">
                                <div class="rating-box__thumb">
                                    <img src="assets/img/organizer.png" alt="Magda Markowska" />
                                </div>
                                <div class="rating-box__content">
                                    <div class="rating-box__name">Magda Markowska</div>
                                    <div class="rating-box__inner">
                                        <div class="rating-box__val">4.5</div>
                                        <div class="rating-box__stars">
                                            <div class="stars">
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star0.5.svg" />
                                            </div>
                                            <div class="rating-box__info">
                                                na podstawie<br/>
                                                <a href="#">15 opinii</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container d-block d-lg-none mt-15">
                <div class="rating-box">
                    <div class="rating-box__thumb">
                        <img src="assets/img/organizer.png" alt="Magda Markowska" />
                    </div>
                    <div class="rating-box__content">
                        <div class="rating-box__name">Magda Markowska</div>
                        <div class="lclz lclz--gray">
                            <div class="lclz__header"><i class="icon-location"></i>Warszawa</div>
                        </div>
                        <div class="rating-box__inner">
                            <div class="rating-box__val rating-box__val--circle">4.5</div>
                            <div class="rating-box__stars">
                                <div class="stars">
                                    <img src="assets/img/star1.0.svg" />
                                    <img src="assets/img/star1.0.svg" />
                                    <img src="assets/img/star1.0.svg" />
                                    <img src="assets/img/star1.0.svg" />
                                    <img src="assets/img/star0.5.svg" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="course-menu" class="course-menu d-none d-lg-block">
                <div class="container">
                    <ul>
                        <li>
                            <a class="active" href="#opis-kursu">Opis kursu</a>
                        </li>
                        <li>
                            <a href="#termin-kursu">Termin</a>
                        </li>
                        <li>
                            <a href="#lokalizacja-kursu">Lokalizacja</a>
                        </li>
                        <li>
                            <a href="#organizator-kursu">Organizator</a>
                        </li>
                    </ul>
                </div>
            </div>

            <main class="container page-content">
                <h3 class="box-header course-desc-header">Opis kursu</h3>
                <div class="row">
                    <div class="col-xs-12 col-lg-8">
                        <section id="opis-kursu" class="box-content course-desc">
                            <div class="course-desc__img d-none d-lg-block">
                                <img src="assets/img/kurs.png" alt="kurs" />
                                <div class="course-desc__cat">Muzyka</div>
                            </div>
                            <div class="course-desc__content">
                                <h4 class="course-desc__header">Informacje o kursie</h4>
                                <div class="course-desc__text">
                                    <p>
                                        Have you ever finally just gave in to the temptation and read your horoscope
                                        in the newspaper on Sunday morning? Sure, we all have. For most of us,
                                        will be like based on the sign of the zodiac that we were born under.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat…
                                        <a class="readmore" href="#">Czytaj dalej <i class="icon-readmore"></i></a>
                                    </p>
                                </div>
                                <div class="skill-list d-none d-lg-block">
                                    <h4>Umiejętności które zdobędziesz</h4>
                                    <ul>
                                        <li>Rozwój osobisty</li>
                                        <li>Słuch</li>
                                        <li>Doskonalenie kordynacji</li>
                                        <li>Przyswojenie podstaw</li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-xs-12 col-lg-4 scrolltofixed">

                        <hr class="separator d-block d-lg-none" />

                        <div class="box-content price-box">
                            <h4>Cena:</h4>
                            <div class="price-box__value">
                                <strong>1200</strong>&nbsp;<span>zł</span>
                            </div>
                            <h5 class="price-box__list-header">Cena objemuje:</h5>
                            <ul class="price-box__list">
                                <li><img src="assets/img/icon-dot-list.svg" alt="icon" />udział w jednodniowym kursie</li>
                                <li><img src="assets/img/icon-dot-list.svg" alt="icon" />konsultacje z ekspertem</li>
                                <li><img src="assets/img/icon-dot-list.svg" alt="icon" />lunch i serwis kawowy</li>
                                <li><img src="assets/img/icon-dot-list.svg" alt="icon" />materiały szkoleniowe</li>
                                <li><img src="assets/img/icon-dot-list.svg" alt="icon" />certyfikat udziału</li>
                            </ul>
                            <p class="price-box__desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                et dolore magna aliqua.
                            </p>
                        </div>

                        <hr class="separator d-block d-lg-none" />

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-8">

                        <section  class="skill-list skill-list--m d-block d-lg-none ">
                            <h4>Umiejętności które zdobędziesz</h4>
                            <ul>
                                <li>Rozwój osobisty</li>
                                <li>Słuch</li>
                                <li>Doskonalenie kordynacji</li>
                                <li>Przyswojenie podstaw</li>
                            </ul>
                        </section>

                        <section id="lokalizacja-kursu" class="row">
                            <div class="col-xs-12 col-lg-6">

                                <hr class="separator d-block d-lg-none" />

                                <div class="box-wrapp box-wrapp--left">
                                    <h3 class="box-header d-none d-lg-flex">Lokalizacja</h3>
                                    <div class="box-content box-content--wpadd">
                                        <h3 class="box-header d-flex d-lg-none">Lokalizacja</h3>
                                        <div class="lclz lclz--black lclz--mb">
                                            <div class="lclz__header lclz__header--bigger d-none d-lg-block"><i class="icon-location"></i>Warszawa</div>
                                            <div class="lclz__header lclz__header--bigger d-block d-lg-none blue"><i class="icon-location"></i>Warszawa</div>
                                        </div>
                                        <p>
                                        Aleje Jerozolimskie 327<br/>
                                        Budynek Accor<br/>
                                        18 piętro
                                        </p>
                                    </div>

                                    <hr class="separator d-block d-lg-none" />

                                </div>
                            </div>
                            <div id="termin-kursu" class="col-xs-12 col-lg-6">
                                <div class="box-wrapp box-wrapp--right">
                                    <h3 class="box-header d-none d-lg-block">Termin</h3>
                                    <div class="box-content box-content--wpadd">
                                        <h3 class="box-header d-block d-lg-none">Termin</h3>
                                        <div class="course-when">
                                            <i class="icon-callendar"></i>
                                            <div>
                                            24-03-2019<br/>
                                            godz. 17:00
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr class="separator d-block d-lg-none" />

                            </div>
                        </section>
                        <section id="organizator-kursu">
                            <h3 class="box-header d-none d-lg-block">Organizator</h3>
                            <div class="box-content organizer">
                                
                                <div class="organizer__inner d-flex d-lg-none">
                                    <h3 class="box-header box-header--organizer-m d-block d-lg-none">Organizator</h3>
                                    <div class="rating-box-m">
                                        <div class="rating-box-m__thumb">
                                            <img src="assets/img/organizer.png" alt="Magda Markowska" />
                                        </div>
                                        <div class="rating-box-m__content">
                                            <div class="rating-box-m__name">Magda Markowska</div>
                                            <div class="lclz lclz--gray">
                                                <div class="lclz__header"><i class="icon-location"></i>Warszawa</div>
                                            </div>
                                            <div class="rating-box-m__inner">
                                                <div class="rating-box-m__val">4.5</div>
                                                <div class="rating-box-m__stars">
                                                    <div class="stars">
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star0.5.svg" />
                                                    </div>
                                                    <div class="rating-box-m__info">
                                                        na podstawie <a href="#">15 opinii</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="organizer__btns">
                                        <div class="btn btn--blue btn--full justify-content-center mb-10 tooltipster" data-tooltip-content="#tooltip1">Kontakt</div>
                                        <a class="btn btn--white btn--full justify-content-center" href="#">
                                            <i class="icon-msg"></i>
                                            Wiadomość
                                        </a>
                                    </div>
                                </div>
                                <div class="organizer__inner d-none d-lg-flex">
                                    <div class="organizer__item">
                                        <div class="organizer__info">
                                            <div class="organizer__img">
                                                <img src="assets/img/organizer.png" alt="Magda Markowska" />
                                            </div>
                                            <div class="organizer__name">
                                                Magda<br/>
                                                Markowska
                                            </div>
                                        </div>
                                    </div>
                                    <div class="organizer__item">
                                        <div class="rating-box__content">
                                            <div class="rating-box__inner">
                                                <div class="rating-box__val rating-box__val--circle">4.5</div>
                                                <div class="rating-box__stars">
                                                    <div class="stars">
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star1.0.svg" />
                                                        <img src="assets/img/star0.5.svg" />
                                                    </div>
                                                    <div class="rating-box__info rating-box__info--gray">
                                                        na podstawie<br/>
                                                        <a href="#">15 opinii</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="organizer__item">
                                        <div class="organizer__btns">
                                            <div class="btn btn--blue btn--full justify-content-center mb-10 tooltipster" data-tooltip-content="#tooltip1">Kontakt</div>
                                            <a class="btn btn--white btn--full justify-content-center" href="#">
                                                <i class="icon-msg"></i>
                                                Wiadomość
                                            </a>
                                            <div class="tooltip-wrapper">
                                                <div id="tooltip1">
                                                    <div class="tooltip">
                                                        <div class="tooltip__header">Kontakt</div>
                                                        <a class="tooltip__item" href="tel:602644706">
                                                            <div class="tooltip__icon">
                                                                <i class="icon-phone"></i>
                                                            </div>
                                                            +48 602 644 706
                                                        </a>
                                                        <a class="tooltip__item tooltip__item--email" href="mailto:602644706">
                                                            <div class="tooltip__icon">
                                                                <i class="icon-address"></i>
                                                            </div>
                                                            adres@email.com
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="organizer__text d-none d-lg-block">
                                    <p>
                                        Have you ever finally just gave in to the temptation and read your horoscope 
                                        in the newspaper on Sunday morning? Sure, we all have. For most of us, will be 
                                        like based on the sign of the zodiac that we were born under.
                                    </p>
                                    <div class="text-right">
                                        <a class="readmore" href="#">Pokaż profil <i class="icon-readmore"></i></a>
                                    </div>
                                </div>

                                <div class="organizer__text-m d-block d-lg-none">
                                    <p>
                                        Have you ever finally just gave in to the temptation and read your horoscope 
                                        in the newspaper on Sunday morning? Sure, we all have. For most of us, will be 
                                        like based on the sign of the zodiac that we were born under.
                                        <div class="text-right">
                                            <a class="readmore" href="#">Pokaż profil <i class="icon-readmore"></i></a>
                                        </div>
                                    </p>
                                </div>

                                <div class="testi-m d-block d-lg-none">
                                    <h4>Opinie uczestników</h4>
                                    <div class="testi-m__inner">
                                        <div class="testi-m__author">
                                            <div class="testi-m__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                        </div>
                                        <div class="testi-m__rating">
                                            <div class="testi-m__author-name">Tomek Nowak</div>
                                            <p class="testi-m__date">22 grudnia 2018</p>
                                            <div class="testi-m__stars stars">
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                            </div>
                                        </div>
                                    </div>
                                    <p class="testi-m__text">
                                        Have you ever finally just gave in to the temptation and read your 
                                        horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                        For most of us, will be like based on the sign of the zodiac that we were born under.
                                    </p>
                                    <div class="text-right">
                                        <a class="readmore" href="#">Zobacz wszystkie <i class="icon-readmore"></i></a>
                                    </div>
                                </div>

                                <div class="testi d-none d-lg-block">
                                    <h4>Opinie uczestników</h4>
                                    <div class="testi__inner">
                                        <div class="testi__author">
                                            <div class="testi__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                            <div class="testi__author-name">Tomek Nowak</div>
                                        </div>
                                        <div>
                                            <div class="testi__rating">
                                                <p class="testi__date">22 grudnia 2018</p>
                                                <div class="testi__stars stars">
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                </div>
                                            </div>
                                            <p class="testi__text">
                                                Have you ever finally just gave in to the temptation and read your 
                                                horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                                For most of us, will be like based on the sign of the zodiac that we were born under.
                                            </p>
                                            <div class="text-right">
                                                <a class="readmore" href="#">Zobacz wszystkie <i class="icon-readmore"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            </main>

            <div class="scrolltofixed-limit"></div>

        </div>

        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

    </body>
</html>
