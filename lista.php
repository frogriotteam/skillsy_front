<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="blue__header">
            <img src="assets/img/music2.png">
            <div class="content">
                <div class="container">
                    <h1>Muzyka - Warszawa</h1>
                    <div class="note">With supporting text below as a natural lead-in to additional content.</div>    
                </div>
            </div>    
        </div>
        <div class="filters-btn" id="filters-btn">
            <span class="txt">Edytuj filtry ogłoszeń</span>
            <span class="num">2</span>
        </div>
        <form action="#" class="list__serach__engine">
            <div id="close-btn"><img src="assets/img/icon_close_black.svg"></div>
            <div class="container">
                 <div class="item">
                     <div class="header">Czego szukasz?</div>
                     <input type="text" class="input__search">
                 </div>
                <div class="item">
                     <div class="header">Wybierz miasto</div>
                     <select class="chosen-select" data-placeholder="Wybierz..." data-no_results_text="Brak wyników">
                         <option value=""></option>
                         <option>Warszawa</option>
                         <option>Kraków</option>
                         <option>Wrocław</option>
                     </select>
                 </div>
                <div class="item">
                     <div class="header">Ocena</div>
                     <select class="chosen-select">
                         <option>4 i więcej</option>
                         <option>3 i więcej</option>
                         <option>2 i więcej</option>
                     </select>
                 </div>
                <div class="item">
                     <div class="header">kategoria</div>
                     <select class="chosen-select"  data-placeholder="Wybierz..."  data-no_results_text="Brak wyników">
                         <option value=""></option>
                         <option>Muzyka</option>
                         <option>Matematyka</option>
                         <option>Historia</option>
                     </select>
                 </div>
                <div class="item">
                     <div class="header">Cena</div>
                     <select class="chosen-select">
                         <option>od 30 do 80 zł</option>
                         <option>od 20 do 60 zł</option>
                         <option>od 40 do 70 zł</option>
                     </select>
                 </div>
                <div class="item">
                    <button type="submit" class="submit"><span class="icon-lupa-icon"></span></button>
                </div>
            </div>
        </form>   
            
        <div class="list__course__wrapper">    

                <div class="list__filters">
                <div class="container">
                    <div class="content">
                        <div class="list__fliters__tabs">
                            <label class="tab-ctn">
                                <input type="radio" name="course" checked>
                                <span class="txt">Domyślnie</span>
                            </label>
                            <label class="tab-ctn">
                                <input type="radio" name="course">
                                <span class="txt">Nadchodzące</span>
                            </label>
                        </div>   
                        <div class="list__filters__pages">
                            <input type="text" class="page" value="1">
                            <div class="txt">z</div>
                            <div class="all__pages">100</div>
                            <button class="arrow"><span class="icon icon-arrow_down"></span></button>
                        </div>
                    </div>    
                </div>    
            </div>
                
                <div class="lists">

                    <div class="list__course__container">
                <div class="container">
                    <div class="header">Oferty sponsorowane</div>
                    <div class="list__course">
                        <a href="#" class="advertisement advertisement--horiz">
                            <div class="advert__head">
                                <div class="advert__img">
                                    <img src="assets/img/img-advert.jpg" alt="">
                                </div>
                                <div class="advert__label">Muzyka</div>
                            </div>
                            <div class="advert__content">
                                <div class="advert__content__row1">
                                    <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                    <div class="location">Warszawa</div>
                                    <div class="advert__details__price">
                                        <div class="price"><span>1100</span>zł</div>
                                    </div>
                                </div>
                                <div class="advert__content__row2">
                                    <div class="profil-rank">
                                        <div class="profil-rank__img">
                                            <img src="assets/img/avatar.jpg" alt="">
                                        </div>
                                        <div class="profil-rank__content">
                                            <div class="profil-rank__name">Adam Mucha</div>
                                            <div class="profil-rank__rank">
                                                <div class="stars two-and-half">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="advert__details">
                                        <div class="header">Termin szkolenia:</div>
                                        <div class="advert__details__data">
                                            <div class="date">24-03-2019, godz. 17:00</div>
                                        </div>
                                    </div>
                                    <div class="advert__skills">
                                        <div class="header">Umiejętności, które zdobędziesz:</div>
                                        <div class="skill">Rozwój osobisty</div>
                                        <div class="skill">Słuch</div>
                                        <div class="skill">Doskonalenie koordynacji</div>
                                    </div>
                                </div>
                            </div>
                        </a>    
                    </div>  
                </div>    
            </div>

            <div class="list__course__container">
                <div class="container">
                    <div class="header">Wszystkie oferty</div>
                    <div class="list__course">
                        <a href="#" class="advertisement advertisement--horiz">
                            <div class="advert__head">
                                <div class="advert__img">
                                    <img src="assets/img/img-advert.jpg" alt="">
                                </div>
                                <div class="advert__label">Muzyka</div>
                            </div>
                            <div class="advert__content">
                                <div class="advert__content__row1">
                                    <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                    <div class="location">Warszawa</div>
                                    <div class="advert__details__price">
                                        <div class="price"><span>1100</span>zł</div>
                                    </div>
                                </div>
                                <div class="advert__content__row2">
                                    <div class="profil-rank">
                                        <div class="profil-rank__img">
                                            <img src="assets/img/avatar.jpg" alt="">
                                        </div>
                                        <div class="profil-rank__content">
                                            <div class="profil-rank__name">Adam Mucha</div>
                                            <div class="profil-rank__rank">
                                                <div class="stars two">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="advert__details">
                                        <div class="header">Termin szkolenia:</div>
                                        <div class="advert__details__data">
                                            <div class="date">24-03-2019, godz. 17:00</div>
                                        </div>
                                    </div>
                                    <div class="advert__skills">
                                        <div class="header">Umiejętności, które zdobędziesz:</div>
                                        <div class="skill">Rozwój osobisty</div>
                                        <div class="skill">Słuch</div>
                                        <div class="skill">Doskonalenie koordynacji</div>
                                    </div>
                                </div>
                            </div>
                        </a>  
                        <a href="#" class="advertisement advertisement--horiz">
                            <div class="advert__head">
                                <div class="advert__img">
                                    <img src="assets/img/img-advert.jpg" alt="">
                                </div>
                                <div class="advert__label">Muzyka</div>
                            </div>
                            <div class="advert__content">
                                <div class="advert__content__row1">
                                    <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                    <div class="location">Warszawa</div>
                                    <div class="advert__details__price">
                                        <div class="price"><span>1100</span>zł</div>
                                    </div>
                                </div>
                                <div class="advert__content__row2">
                                    <div class="profil-rank">
                                        <div class="profil-rank__img">
                                            <img src="assets/img/avatar.jpg" alt="">
                                        </div>
                                        <div class="profil-rank__content">
                                            <div class="profil-rank__name">Adam Mucha</div>
                                            <div class="profil-rank__rank">
                                                <div class="stars one-and-half">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="advert__details">
                                        <div class="header">Termin szkolenia:</div>
                                        <div class="advert__details__data">
                                            <div class="date">24-03-2019, godz. 17:00</div>
                                        </div>
                                    </div>
                                    <div class="advert__skills">
                                        <div class="header">Umiejętności, które zdobędziesz:</div>
                                        <div class="skill">Rozwój osobisty</div>
                                        <div class="skill">Słuch</div>
                                        <div class="skill">Doskonalenie koordynacji</div>
                                    </div>
                                </div>
                            </div>
                        </a>  
                        <a href="#" class="advertisement advertisement--horiz">
                            <div class="advert__head">
                                <div class="advert__img">
                                    <img src="assets/img/img-advert.jpg" alt="">
                                </div>
                                <div class="advert__label">Muzyka</div>
                            </div>
                            <div class="advert__content">
                                <div class="advert__content__row1">
                                    <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                    <div class="location">Warszawa</div>
                                    <div class="advert__details__price">
                                        <div class="price"><span>1100</span>zł</div>
                                    </div>
                                </div>
                                <div class="advert__content__row2">
                                    <div class="profil-rank">
                                        <div class="profil-rank__img">
                                            <img src="assets/img/avatar.jpg" alt="">
                                        </div>
                                        <div class="profil-rank__content">
                                            <div class="profil-rank__name">Adam Mucha</div>
                                            <div class="profil-rank__rank">
                                                <div class="stars three-and-half">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="advert__details">
                                        <div class="header">Termin szkolenia:</div>
                                        <div class="advert__details__data">
                                            <div class="date">24-03-2019, godz. 17:00</div>
                                        </div>
                                    </div>
                                    <div class="advert__skills">
                                        <div class="header">Umiejętności, które zdobędziesz:</div>
                                        <div class="skill">Rozwój osobisty</div>
                                        <div class="skill">Słuch</div>
                                        <div class="skill">Doskonalenie koordynacji</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>    
                </div>    
            </div>
                    
                </div>  
            
                <div class="container">
                    
                    <div class="course__list__footer">
                        
                        <div class="numbers">
                            1 - <span>60</span> z <span>6000</span> kursów
                        </div>
            
                        <nav aria-label="navigation">
                        <ul class="pagination justify-content-end">
                            <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1"><span class="icon icon-arrow-left"></span>Poprzednia</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link page-link--active" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">Następna<span class="icon icon-arrow-right"></span></a>
                        </li>
                        </ul>
                    </nav>
                        
                    </div>    
                    
                </div>    
                
            </div>    
        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
