<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="login-wrapper content__container">
                    <div class="login side">
                        <h1 class="header__level__2">Zaloguj się</h1>
                        <a href="#" class="fb-button btn-wide"><span class="icon icon-facebook"></span>Zaloguj przez facebooka</a>
                        <div class="gap"><span>lub</span></div>
                        <form id="form" class="form">
                            <div class="form__box">
                                <label class="label-box">E-mail lub nazwa użytkownika</label>
                                <input type="text" name="name">
                            </div>  
                            <div class="form__box error">
                                <label class="label-box error">Hasło</label>
                                <input type="password" name="password">
                                <span class="error-txt">Podałeś niepoprawne hasło</span>
                            </div> 
                            <button type="submit" class="btn btn-wide login-btn">Zaloguj</button>
                            <a href="#" class="forgot-pass">Nie pamiętasz hasła</a>
                        </form>
                    </div>
                    <div class="signup side">
                        <h1 class="header__level__2">Załóż darmowe konto</h1>
                        <img src="assets/img/singup.svg" alt="" class="image">
                        <a href="#" class="btn btn--red btn-wide">Zarejestruj się za darmo</a>
                    </div>
                </div>

        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
