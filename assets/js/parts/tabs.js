const tabs = () => {
    
    $('.tab_content').hide();
    $('.tab_content:first').show();
    $('.tabs li:first').addClass('active');
    $('.tabs li a').click(function(event) {
        event.preventDefault()
        
    $('.tabs li').removeClass('active');
    $(this).parent().addClass('active');
    $('.tab_content').hide();

    var selectTab = $(this).attr("href");

    $(selectTab).fadeIn();
    });
    
 }



 module.exports = {
     tabs
 };