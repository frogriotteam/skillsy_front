<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="checkout">
                <div class="checkout__head">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <div class="checkout__head__top">
                                <a href="#" class="prev-btn"><img src="assets/img/arrow-left_blue.svg" alt="">Wróć</a>
                                <a href="/" class="logo"><img src="assets/img/logo.svg" alt=""></a>
                                <div class="note"><img src="assets/img/lock.svg" alt=""><span>Bezpieczne płatności</span></div>
                            </div>
                            <div class="order">
                                <h2 class="header header__level__2">Sfinalizuj zamówienie</h2>
                                <div class="advertisement advertisement--horiz advertisement--checkout">
                                    <div class="advert__head">
                                        <div class="advert__img">
                                            <img src="assets/img/img-advert.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="advert__content">
                                        <div class="advert__content__row1">
                                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                        </div>
                                        <div class="advert__content__row2">
                                            <div class="advert__skills">
                                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                                <div class="skill">Rozwój osobisty</div>
                                                <div class="skill">Słuch</div>
                                                <div class="skill">Doskonalenie</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="checkout__content">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <form id="checkout-form" class="form">
                                <h2 class="header__level__3">Wybierz ilość lekcji</h2>
                                <table class="lessons">
                                    <tr>
                                        <td>
                                            <label class="radio-container">
                                                <input type="radio" name="lesson" checked>
                                                <span class="state"></span>
                                            </label>    
                                        </td>
                                        <td class="lesson-cell"><div class="lesson">1 lekcja</div></td>
                                        <td></td>
                                        <td class="multiply-cell"><div class="multiply">1x</div></td>
                                        <td class="price-cell"><div class="price">110zł/h</div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="radio-container">
                                                <input type="radio" name="lesson">
                                                <span class="state"></span>
                                            </label>  
                                        </td>
                                        <td class="lesson-cell"><div class="lesson">5 lekcji</div></td>
                                        <td><div class="promo">10% TANIEJ</div></td>
                                        <td class="multiply-cell"><div class="multiply">5x</div></td>
                                        <td class="price-cell"><div class="price">100zł/h</div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="radio-container">
                                                <input type="radio" name="lesson">
                                                <span class="state"></span>
                                            </label>  
                                        </td>
                                        <td class="lesson-cell"><div class="lesson">10 lekcji</div></td>
                                        <td><div class="promo">13% TANIEJ</div></td>
                                        <td class="multiply-cell"><div class="multiply">10x</div></td>
                                        <td class="price-cell"><div class="price">98zł/h</div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="radio-container">
                                                <input type="radio" name="lesson">
                                                <span class="state"></span>
                                            </label>  
                                        </td>
                                        <td class="lesson-cell"><div class="lesson">20 lekcji</div></td>
                                        <td><div class="promo">13% TANIEJ</div></td>
                                        <td class="multiply-cell"><div class="multiply">20x</div></td>
                                        <td class="price-cell"><div class="price">98zł/h</div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="radio-container">
                                                <input type="radio" name="lesson">
                                                <span class="state"></span>
                                            </label>  
                                        </td>
                                        <td class="lesson-cell"><div class="lesson">50 lekcji</div></td>
                                        <td><div class="promo">13% TANIEJ</div></td>
                                        <td class="multiply-cell"><div class="multiply">50x</div></td>
                                        <td class="price-cell"><div class="price">98zł/h</div></td>
                                    </tr>
                                </table>
                                <div class="lessons_summary">
                                    <div class="txt">Razem:</div>
                                    <div class="price">500zł</div>
                                </div>
                                <h2 class="header__level__3">Wybierz sposób płatności</h2>
                                <div class="payments">
                                    <label class="item">
                                        <input type="radio" name="payment">
                                        <div class="content">
                                            <span class="state"></span>
                                            <div class="icon">
                                                <img src="assets/img/payment-01.png" alt="">
                                            </div>    
                                        </div>
                                    </label>
                                    <label class="item">
                                        <input type="radio" name="payment">
                                        <div class="content">
                                            <span class="state"></span>
                                            <div class="icon">
                                                <img src="assets/img/payment-02.png" alt="">
                                                <div class="txt">Karta kredytowa</div>
                                            </div>    
                                        </div>
                                    </label>
                                </div>
                                <div class="center">
                                    <button type="submit" class="btn">Opłać zamówienie</button>
                                </div>    
                            </form>    
                        </div>
                    </div>    
                </div>
            </div>
            
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
