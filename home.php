<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>
        <section class="main-banner">
            <img class="main-banner__bg" src="assets/img/main_banner.png" alt="">
            <div class="main-banner__container">
                <div class="main-banner__inner">
                    <h1>Naucz się czegoś nowego.</h1>
                    <div class="note">Przeszukaj <span class="num">2105</span> kursów</div>
                    <form class="search-engine">
                        <div class="search-engine__col">
                            <span class="header">Czego szukasz?</span>
                            <input type="text" name="what" id="what">
                        </div>
                        <div class="search-engine__col">
                            <span class="header">Wybierz miasto</span>
                            <select class="chosen-select">
                                <option>Warszawa</option>
                                <option>Kraków</option>
                                <option>Wrocław</option>
                            </select>
                        </div>
                        <div class="search-engine__col">
                            <button type="submit" class="submit-btn btn"><span class="icon icon-lupa-icon"></span><span class="txt">Szukaj</span></button>
                        </div>
                    </form>
                </div>    
            </div>
        </section>
        
        <section class="popular_categories">
            <header class="section-header">
                <h2>Najpopularniejsze kategorie</h2>
            </header>
            <div class="categtories" id="categories__slider">
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/photo.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Fotografia</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/photo.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Fotografia</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/camera.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Filmowanie</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/screen.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Programowanie</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/camera.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Fotografia</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/photo.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Fotografia</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/photo.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Fotografia</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/camera.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Filmowanie</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/screen.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Programowanie</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
                <div class="category">
                    <div class="icon-box">
                        <img src="assets/img/camera.svg" alt="">
                    </div>
                    <div class="content-box">
                        <div class="header">Fotografia</div>
                        <div class="number-box">
                            <div class="number">4235</div>kursów
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <div class="center">
            <button class="go"><div class="icon-scroll"><img src="assets/img/scroll.svg" alt=""></div><span class="txt">Przeglądaj dalej</span></button>
        </div>
        
        <section class="advertisements">
            <header class="section-header">
                <h2>Wyróżnione ogłoszenia</h2>
                <div class="slider-nav">
                    <button id="advert-prev" class="advert-nav"><i class="icon-arrow-right"></i></button>
                    <button id="advert-next" class="advert-nav"><i class="icon-arrow-right"></i></button>
                </div>
            </header>
            <div class="content">
                <div class="adverts--list--horiz" id="advert-slider">
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars five">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars four-and-half">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars four">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars three-and-half">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars three">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars two-and-half">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars two">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars one-and-half">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars one">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="advertisement">
                        <div class="advert__head">
                            <div class="advert__img">
                                <img src="assets/img/img-advert.jpg" alt="">
                            </div>
                            <div class="advert__label">Muzyka</div>
                            <div class="profil-rank">
                                <div class="profil-rank__img">
                                    <img src="assets/img/avatar.jpg" alt="">
                                </div>
                                <div class="profil-rank__content">
                                    <div class="profil-rank__name">Adam Mucha</div>
                                    <div class="profil-rank__rank">
                                        <div class="stars half">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advert__content">
                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                            <div class="advert__details">
                                <div class="advert__details__data">
                                    <div class="date">24-03-2019</div>
                                    <div class="location">Warszawa</div>
                                </div>
                                <div class="advert__details__price">
                                    <div class="price"><span>1100</span>zł</div>
                                </div>
                            </div>
                            <div class="advert__skills">
                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                <div class="skill">Rozwój osobisty</div>
                                <div class="skill">Słuch</div>
                                <div class="skill">Doskonalenie koordynacji</div>
                            </div>
                        </div>
                    </a>
                </div> 
            </div>    
            <div class="center">
                <a href="#" class="btn-with-arrow"><span class="txt">Zobacz wszystkie</span><span class="icon icon-arrow_grey"></span></a>
            </div>   
        </section>
        
        <section class="how_it_works">
            <header class="section-header">
                <h2>Jak to działa?</h2>
                <div class="center">
                    <ul class="tabs">
                        <li class="tab active"><a href="#tab1">
                            Chcę się nauczyć czegoś nowego
                        </a></li>
                        <li class="tab"><a href="#tab2">
                            Chcę się podzielić tym co umiem
                        </a></li>
                    </ul>
                </div>    
            </header>
            <div class="tab_container">
              <div id="tab1" class="tab_content">
                <div class="content">
                    <div class="img-box">
                        <img src="assets/img/img-how_it_works.jpg" 0 0 no-repeat;>
                    </div>
                    <div class="content-box">
                        <div class="steps">
                            <div class="step">
                                <div class="icon-box">
                                    <div class="icon icon-icon-kurs"></div>
                                </div>
                                <div class="content-box">
                                    <div class="count">Krok 1</div>
                                    <div class="name">Wyszukaj kurs</div>
                                    <div class="desc">Sed congue vulputate nibh, vel pharetra ex lacinia at</div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="icon-box">
                                    <div class="icon icon-icon_termin"></div>
                                </div>
                                <div class="content-box">
                                    <div class="count">Krok 2</div>
                                    <div class="name">Umów termin</div>
                                    <div class="desc">Maecenas id vehicula sapien, eget gravida lectus. Cras porta scelerisque cras porta scelerisque luctus.</div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="icon-box">
                                    <div class="icon icon-icona_skill"></div>
                                </div>
                                <div class="content-box">
                                    <div class="count">Krok 3</div>
                                    <div class="name">Zdobądź skilla</div>
                                    <div class="desc">Nam tincidunt risus vitae ipsum varius, malesuada auctor dolor scelerisque. Etiam eyuismod.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
              </div>
              <div id="tab2" class="tab_content">
                <div class="content">
                    <div class="img-box">
                        <img src="assets/img/img-how_it_works_2.jpg" 0 0 no-repeat;>
                    </div>
                    <div class="content-box">
                        <div class="steps">
                            <div class="step">
                                <div class="icon-box">
                                    <div class="icon icon-icon_how_01"></div>
                                </div>
                                <div class="content-box">
                                    <div class="count">Krok 1</div>
                                    <div class="name">Załóż konto</div>
                                    <div class="desc">Sed congue vulputate nibh, vel pharetra ex lacinia at</div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="icon-box">
                                    <div class="icon icon-icon_how_02"></div>
                                </div>
                                <div class="content-box">
                                    <div class="count">Krok 2</div>
                                    <div class="name">Stwórz ogłoszenie</div>
                                    <div class="desc">Maecenas id vehicula sapien, eget gravida lectus. Cras porta scelerisque cras porta scelerisque luctus.</div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="icon-box">
                                    <div class="icon icon-icon_how_03"></div>
                                </div>
                                <div class="content-box">
                                    <div class="count">Krok 3</div>
                                    <div class="name">Zbieraj zgłoszenia</div>
                                    <div class="desc">Nam tincidunt risus vitae ipsum varius, malesuada auctor dolor scelerisque. Etiam eyuismod.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>    
        </section>
        
        <section class="about_us">
            <div class="container">
                <header class="section-header header-white">
                    <h2>O nas</h2>
                </header>
                <div class="content">
                    <div class="text-box">
                        <h3 class="header">Jesteśmy tu dla Was!</h3>
                        <p>
                            Proin in justo id purus molestie sagit tis.
                            Praesent consequat lobor tis consequat.
                            Curabitur a placerat nisl. Integer condimentum congue scelerisque. Quisque vel nulla faucibus. scelerisque purus vehicula, iacullis mauris. Nullam efficitur tristique fermentum. Ut sed nisl ac ante scelerisque pretium.
                        </p>
                    </div>
                    <div class="image-box">
                        <img src="assets/img/illustracja.svg" alt="">
                    </div>
                </div> 
            </div>    
        </section>
        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
