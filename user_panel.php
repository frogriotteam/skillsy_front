<?php include 'include/head.php' ?>

    <?php include 'include/header.php' ?>
            
    <div class="body-wrapper body-wrapper--gray-m">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-lg-3">

                    <nav class="panel-nav">
                        <div class="panel-nav__title">
                            <span>Panel zarządzania</span>
                            <img src="assets/fonts/svg/arrow-down.svg" />
                        </div>
                        <ul class="panel-nav__list">
                            <li>
                                <a class="add-item" href="#">
                                    <span class="add-item__icon add-item__icon--big"></span>
                                    <span class="add-item__text">Dodaj ogłoszenie</span>
                                </a>
                            </li>
                            <li>
                                <a class="active" href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-star-black"></i>
                                    </span>
                                    <span>Ogłoszenia</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-ok"></i>
                                    </span>
                                    <span>Opinie</span>
                                    <span class="panel-nav__counter">15</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-msg"></i>
                                    </span>
                                    <span>Czat</span>
                                    <span class="panel-nav__counter">3</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-acc"></i>
                                    </span>
                                    <span>Twoje dane</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-icon_baknot"></i>
                                    </span>
                                    <span>Historie płatności</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    
                </div>

                <div class="col-xs-12 col-lg-9">

                    <div class="panel-hello">
                        <div class="panel-hello__avatar d-block d-lg-none">
                            <img src="assets/img/avatar.png" alt="avatar" />
                        </div>
                        <div class="panel-hello__text">Witaj, <span class="blue">Adam</span></div>
                        <a class="panel-hello__btn" href="#">Edytuj dane</a>
                    </div>
                    <div class="panel-content">

                        <div id="panel-nav-slider"></div>

                        <div class="highlight-box">
                            <div class="highlight-box__inner">
                                <div class="highlight-box__title">Wyróżnij swoje ogłoszenie</div>
                                <ul class="highlight-box__list">
                                    <li><img src="assets/fonts/svg/check.svg" />3 razy więcej odwiedzających</li>
                                    <li><img src="assets/fonts/svg/check.svg" />Zarabiaj więcej</li>
                                    <li><img src="assets/fonts/svg/check.svg" />Tylko 4,99 zł</li>
                                </ul>
                            </div>
                        </div>

                        <header class="panel-content__header">
                            <h2>Moje ogłoszenia</h2>
                            <a class="add-item d-flex d-lg-none" href="#">
                                <span class="add-item__icon"></span>
                                <span class="add-item__text">Dodaj ogłoszenie</span>
                            </a>
                        </header>

                        <div class="cfs">

                            <div class="cfs__inner">

                                <div class="cfs__item">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-3">

                                            <div class="cfs-img">
                                                <img src="assets/img/img-advert.jpg" alt="">
                                                <div class="cfs-img__cat">MUZYKA</div>
                                                <div class="cfs-img__title">
                                                    Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-lg-9">

                                            <div class="cfs__content">

                                                <div class="row d-none d-lg-flex">
                                                    <div class="col-xs-12 col-lg-8 col-xl-9">
                                                        <h2 class="cfs__title">
                                                            Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat
                                                        </h2>
                                                    </div>
                                                    <div class="col-xs-12 col-lg-4 col-xl-3">
                                                        <div class="cfs__is-public">
                                                            <div class="switch d-none d-lg-flex">
                                                                <label class="switch__inner">
                                                                    <input class="switch__input" type="checkbox" checked>
                                                                    <span class="switch__slider"></span>
                                                                </label>
                                                                <div class="switch__content">Publiczne</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row d-none d-lg-flex">
                                                    <div class="col-lg-12">
                                                        <div class="cfs__options">
                                                            <a href="#">Edytuj</a>
                                                            <a href="#">Skasuj</a>
                                                            <a href="#">Zobacz</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="switch d-flex d-lg-none">
                                                    <label class="switch__inner">
                                                        <input class="switch__input" type="checkbox">
                                                        <span class="switch__slider"></span>
                                                    </label>
                                                    <div class="switch__content">Publiczne</div>
                                                </div>

                                                <button class="btn btn--white btn--full justify-content-center d-flex d-lg-none btn--manage" data-tooltip-content="#tooltip1">
                                                    <i class="icon-manage"></i>
                                                    Zarządzaj
                                                </button>

                                                <div class="tooltip-wrapper">
                                                    <div id="tooltip1">
                                                        <div class="tooltip2 container">
                                                            <div class="tooltip2__inner">
                                                                <div class="tooltip2__close"></div>
                                                                <div class="tooltip2__header">Zarządzaj ogłoszeniem</div>
                                                                <ul>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-edit"></i>
                                                                        </span>
                                                                        Edytuj
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-delete"></i>
                                                                        </span>
                                                                        Skasuj
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-eye"></i>
                                                                        </span>
                                                                        Zobacz
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr class="separator d-block d-lg-none">

                                                <div class="row cfs__btns">
                                                    <div class="col-xs-12 col-lg-6 cfs__btn cfs__btn--border cfs__btn--border-right">
                                                        <span>Wyróżnij<br/> graficznie</span>
                                                        <a class="btn btn--red2" href="#">
                                                            <img src="assets/fonts/svg/star2.svg" />
                                                            4,90 zł
                                                        </a>
                                                        <hr class="separator d-block d-lg-none">
                                                    </div>
                                                    <div class="col-xs-12 col-lg-6 cfs__btn">
                                                        <span>Przesuń na szczyt<br/> listy wyszukiwań</span>
                                                        <a class="btn btn--red2" href="#">
                                                            <img src="assets/fonts/svg/arrow-up.svg" />
                                                            4,90 zł
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <hr class="separator d-block d-lg-none">

                                <div class="cfs__item">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-3">

                                            <div class="cfs-img">
                                                <img src="assets/img/img-advert.jpg" alt="">
                                                <div class="cfs-img__cat">MUZYKA</div>
                                                <div class="cfs-img__title">
                                                    Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-lg-9">

                                            <div class="cfs__content">

                                                <div class="row d-none d-lg-flex">
                                                    <div class="col-xs-12 col-lg-8 col-xl-9">
                                                        <h2 class="cfs__title">
                                                            Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat
                                                        </h2>
                                                    </div>
                                                    <div class="col-xs-12 col-lg-4 col-xl-3">
                                                        <div class="cfs__is-public">
                                                            <div class="switch d-none d-lg-flex">
                                                                <label class="switch__inner">
                                                                    <input class="switch__input" type="checkbox" checked>
                                                                    <span class="switch__slider"></span>
                                                                </label>
                                                                <div class="switch__content">Publiczne</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row d-none d-lg-flex">
                                                    <div class="col-lg-12">
                                                        <div class="cfs__options">
                                                            <a href="#">Edytuj</a>
                                                            <a href="#">Skasuj</a>
                                                            <a href="#">Zobacz</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="switch d-flex d-lg-none">
                                                    <label class="switch__inner">
                                                        <input class="switch__input" type="checkbox">
                                                        <span class="switch__slider"></span>
                                                    </label>
                                                    <div class="switch__content">Publiczne</div>
                                                </div>

                                                <button class="btn btn--white btn--full justify-content-center d-flex d-lg-none btn--manage" data-tooltip-content="#tooltip2">
                                                    <i class="icon-manage"></i>
                                                    Zarządzaj
                                                </button>

                                                <div class="tooltip-wrapper">
                                                    <div id="tooltip2">
                                                        <div class="tooltip2">
                                                            <div class="tooltip2__inner">
                                                                <div class="tooltip2__close"></div>
                                                                <div class="tooltip2__header">Zarządzaj ogłoszeniem</div>
                                                                <ul>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-edit"></i>
                                                                        </span>
                                                                        Edytuj
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-delete"></i>
                                                                        </span>
                                                                        Skasuj
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-eye"></i>
                                                                        </span>
                                                                        Zobacz
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr class="separator d-block d-lg-none">

                                                <div class="row cfs__btns">
                                                    <div class="col-xs-12 col-lg-6 cfs__btn cfs__btn--border cfs__btn--border-right">
                                                        <span>Wyróżnij<br/> graficznie</span>
                                                        <a class="btn btn--red2" href="#">
                                                            <img src="assets/fonts/svg/star2.svg" />
                                                            4,90 zł
                                                        </a>
                                                        <hr class="separator d-block d-lg-none">
                                                    </div>
                                                    <div class="col-xs-12 col-lg-6 cfs__btn">
                                                        <span>Przesuń na szczyt<br/> listy wyszukiwań</span>
                                                        <a class="btn btn--red2" href="#">
                                                            <img src="assets/fonts/svg/arrow-up.svg" />
                                                            4,90 zł
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <hr class="separator d-block d-lg-none">

                                <div class="cfs__item cfs__item--last">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-3">

                                            <div class="cfs-img">
                                                <img src="assets/img/img-advert.jpg" alt="">
                                                <div class="cfs-img__cat">MUZYKA</div>
                                                <div class="cfs-img__title">
                                                    Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-lg-9">

                                            <div class="cfs__content">

                                                <div class="row d-none d-lg-flex">
                                                    <div class="col-xs-12 col-lg-8 col-xl-9">
                                                        <h2 class="cfs__title">
                                                            Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat
                                                        </h2>
                                                    </div>
                                                    <div class="col-xs-12 col-lg-4 col-xl-3">
                                                        <div class="cfs__is-public">
                                                            <div class="switch d-none d-lg-flex">
                                                                <label class="switch__inner">
                                                                    <input class="switch__input" type="checkbox">
                                                                    <span class="switch__slider"></span>
                                                                </label>
                                                                <div class="switch__content">Publiczne</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row d-none d-lg-flex">
                                                    <div class="col-lg-12">
                                                        <div class="cfs__options">
                                                            <a href="#">Edytuj</a>
                                                            <a href="#">Skasuj</a>
                                                            <a href="#">Zobacz</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="switch d-flex d-lg-none">
                                                    <label class="switch__inner">
                                                        <input class="switch__input" type="checkbox">
                                                        <span class="switch__slider"></span>
                                                    </label>
                                                    <div class="switch__content">Publiczne</div>
                                                </div>

                                                <button class="btn btn--white btn--full justify-content-center d-flex d-lg-none btn--manage" data-tooltip-content="#tooltip3">
                                                    <i class="icon-manage"></i>
                                                    Zarządzaj
                                                </button>

                                                <div class="tooltip-wrapper">
                                                    <div id="tooltip3">
                                                        <div class="tooltip2">
                                                            <div class="tooltip2__inner">
                                                                <div class="tooltip2__close"></div>
                                                                <div class="tooltip2__header">Zarządzaj ogłoszeniem</div>
                                                                <ul>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-edit"></i>
                                                                        </span>
                                                                        Edytuj
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-delete"></i>
                                                                        </span>
                                                                        Skasuj
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                        <span class="tooltip2__icon">
                                                                            <i class="icon-eye"></i>
                                                                        </span>
                                                                        Zobacz
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr class="separator d-block d-lg-none">

                                                <div class="row cfs__btns">
                                                    <div class="col-xs-12 col-lg-6 cfs__btn cfs__btn--border cfs__btn--border-right">
                                                        <span>Wyróżnij<br/> graficznie</span>
                                                        <a class="btn btn--red2" href="#">
                                                            <img src="assets/fonts/svg/star2.svg" />
                                                            4,90 zł
                                                        </a>
                                                        <hr class="separator d-block d-lg-none">
                                                    </div>
                                                    <div class="col-xs-12 col-lg-6 cfs__btn">
                                                        <span>Przesuń na szczyt<br/> listy wyszukiwań</span>
                                                        <a class="btn btn--red2" href="#">
                                                            <img src="assets/fonts/svg/arrow-up.svg" />
                                                            4,90 zł
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <hr class="separator d-block d-lg-none">

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-lg-9 offset-lg-3">
                    <div class="course__list__footer course__list__footer--padd">
                        
                        <div class="numbers">
                            1 - <span>3</span> z <span>3</span> kursów
                        </div>
            
                        <nav aria-label="navigation">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1"><span class="icon icon-arrow-left"></span>Poprzednia</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link page-link--active" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                <a class="page-link" href="#">Następna<span class="icon icon-arrow-right"></span></a>
                            </li>
                            </ul>
                        </nav>
                        
                    </div>
                </div>
            </div>

        </div>

    </div>

    <?php include 'include/footer.php' ?>

    <?php include 'include/javascript.php' ?>

    </body>
</html>
