<footer id="footer" class="page-footer">
    <div class="container">
        <div class="row-1">
            <div class="logo-footer">
                <img src="assets/img/logo_white.svg" alt="">
            </div>
            <div class="footer-links">
                <div class="col">
                    <div class="header">Kursy</div>
                    <ul class="list">
                        <li><a href="#">Język angielski</a></li>
                        <li><a href="#">Fotografia</a></li>
                        <li><a href="#">Matematyka</a></li>
                        <li><a href="#">Programowanie</a></li>
                    </ul>
                </div>
                <div class="col">
                    <div class="header">O serwisie</div>
                    <ul class="list">
                        <li><a href="#">Polityka prywatności</a></li>
                        <li><a href="#">O nas</a></li>
                        <li><a href="#">Kontakt</a></li>
                        <li><a href="#">Regulamin</a></li>
                    </ul>
                </div>
                <div class="col">
                    <div class="header">Konto</div>
                    <ul class="list">
                        <li><a href="#">Logowanie</a></li>
                        <li><a href="#">Rejestracja</a></li>
                    </ul>
                </div>
            </div>
            <div class="socials-footer">
                <div class="header">Dołącz do nas</div>
                <div class="content">
                    <a href="#" class="icon-box">
                        <span class="icon icon-icon-fb"></span>
                    </a>
                    <a href="#" class="icon-box">
                        <span class="icon icon-icon-insta"></span>
                    </a>
                    <a href="#" class="icon-box">
                        <span class="icon icon-icon-in"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row-2">
            <span class="copy">&copy; 2019 Skillsy. All rights reserved</span>
            <div class="page-maker">
                <span class="txt">Wykonanie</span>
                <a target="_blank" href="http://www.frogriot.com"><img src="assets/img/logo-frogriot.svg" alt=""></a>
            </div>
        </div>
    </div>
</footer>