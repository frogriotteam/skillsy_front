<?php include 'include/head.php' ?>

    <?php include 'include/header.php' ?>
            
    <div class="body-wrapper body-wrapper--gray-m body-wrapper--pb-100">

        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-lg-3">

                    <nav class="panel-nav">
                        <div class="panel-nav__title">
                            <span>Panel zarządzania</span>
                            <img src="assets/fonts/svg/arrow-down.svg" />
                        </div>
                        <ul class="panel-nav__list">
                            <li>
                                <a class="add-item" href="#">
                                    <span class="add-item__icon add-item__icon--big"></span>
                                    <span class="add-item__text">Dodaj ogłoszenie</span>
                                </a>
                            </li>
                            <li>
                                <a class="active" href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-star-black"></i>
                                    </span>
                                    <span>Ogłoszenia</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-ok"></i>
                                    </span>
                                    <span>Opinie</span>
                                    <span class="panel-nav__counter">15</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-msg"></i>
                                    </span>
                                    <span>Czat</span>
                                    <span class="panel-nav__counter">3</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-acc"></i>
                                    </span>
                                    <span>Twoje dane</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="panel-nav__icon">
                                        <i class="icon-icon_baknot"></i>
                                    </span>
                                    <span>Historie płatności</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    
                </div>

                <div class="col-xs-12 col-lg-9">

                    <div class="panel-hello">
                        <div class="panel-hello__avatar d-block d-lg-none">
                            <img src="assets/img/avatar.png" alt="avatar" />
                        </div>
                        <div class="panel-hello__text">Witaj, <span class="blue">Adam</span></div>
                        <a class="panel-hello__btn" href="#">Edytuj dane</a>
                    </div>

                    <div class="panel-content">

                        <header class="panel-content__header">
                            <h2>Historia płatności</h2>
                        </header>

                        <div class="hpayments">

                            <div class="hpayments__item hpayments__item--label d-none d-lg-flex">
                                <div class="hpayments__label hpayments__label--id">Id transakcji</div>
                                <div class="hpayments__label hpayments__label--title">Nazwa ogłoszenia</div>
                                <div class="hpayments__label hpayments__label--date">Data</div>
                                <div class="hpayments__label">Cena</div>
                                <div class="hpayments__label"></div>
                            </div>
                            
                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                            <div class="hpayments__item">
                                <div class="hpayments__col hpayments__col--id">1298</div>
                                <div class="hpayments__col hpayments__col--title">Kurs gry na gitarze - kurs podstawowy…</div>
                                <div class="hpayments__col hpayments__col--date">24-03-2019</div>
                                <div class="hpayments__col hpayments__col--price">4,90 zł</div>
                                <div class="hpayments__col hpayments__col--btn"><a class="hpayments__btn" href="#">Wygeneruj e-fakturę</a></div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-lg-9 offset-lg-3">
                    <div class="course__list__footer course__list__footer--padd">
                        
                        <div class="numbers">
                            1 - <span>3</span> z <span>3</span> kursów
                        </div>
            
                        <nav aria-label="navigation">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1"><span class="icon icon-arrow-left"></span>Poprzednia</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link page-link--active" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                <a class="page-link" href="#">Następna<span class="icon icon-arrow-right"></span></a>
                            </li>
                            </ul>
                        </nav>
                        
                    </div>
                </div>
            </div>

        </div>

    </div>

    <?php include 'include/footer.php' ?>

    <?php include 'include/javascript.php' ?>

    </body>
</html>
