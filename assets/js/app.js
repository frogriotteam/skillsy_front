//--general

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import {
    announce_form
} from './parts/form_dodawanie_ogloszenia.js';
import {
    carousels
} from './parts/carousels.js';
import {
    tabs
} from './parts/tabs.js';
import {
    footerAccordion
} from './parts/footer_accordion.js';
import {
    smooth_scroll
} from './parts/smooth_scroll.js';
import flatpickr from "flatpickr"
import {
    Polish
} from "flatpickr/dist/l10n/pl.js"

import {
    slidingMenu
} from './parts/slide_menu';
import {
    scrollToFixed
} from './parts/scroll_to_fixed';
import scrollSpy from 'simple-scrollspy'
import 'bootstrap/js/dist/collapse';


require("chosen-js");
require("tooltipster");
require("jfilestyle");
require("slick-carousel");
require("scrolltofixed");
require("cropper");
require("bootstrap-tagsinput");
require("raty-js");
require("scrollnav");





(function ($) {

    //-- delay function for resizing
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        }
    })();

    $("#filters-btn").click(function(){
        $('.list__serach__engine').addClass('show')
    })

    $("#close-btn").click(function(){
        $('.list__serach__engine').removeClass('show')
    })
        
    $(document).ready(function () {

        $(".chosen-select").chosen()

        carousels();

        tabs();

        footerAccordion();

        smooth_scroll();

        flatpickr.localize(Polish);

        flatpickr($("#date"), {
            "locale": Polish
        });

        flatpickr($("#time"), {
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            time_24hr: true
        });

        var $image = $('#image');

        $image.cropper();


        $.fn.raty.defaults.starOn = 'assets/img/star1.0.svg';
        $.fn.raty.defaults.starOff = 'assets/img/star0.svg';
        $.fn.raty.defaults.starHalf = 'assets/img/star0.5.svg';

        $('#raty').raty({
            half: true,
            halfShow: true,
            precision: true,
            target: '#target',
            targetType: 'number',
            targetKeep: true
        });

        $('.profile__order .profile__tabs').scrollToFixed()





        $('#term-check-1').click(function () {
            $(this).parents('.form-radio').addClass('content-1').removeClass('content-2')
        })

        $('#term-check-2').click(function () {
            $(this).parents('.form-radio').addClass('content-2').removeClass('content-1')
        })



        if ($(window).width() > 1200) {
            $('.list__serach__engine').scrollToFixed()
        }

        $('.tooltipster').tooltipster({
            theme: 'tooltipster-noir',
            trigger: 'click',
            contentAsHTML: true,
            interactive: true,
            distance: -40,
        });

        $('.btn--manage').tooltipster({
            theme: 'tooltipster-shadow',
            trigger: 'click',
            contentAsHTML: true,
            interactive: true,
            distance: 0,
            arrow: false,
            side: ['bottom'],
            functionPosition: function (instance, helper, position) {
                position.coord.top -= 55;
                return position;
            },
            functionReady: function (instance) {
                $('.tooltip2__close').click(function () {
                    instance.close();
                });
            }
        });

        $('.hamburger').on('click', function (e) {
            e.preventDefault();
            var menu_attr = $(this).attr('data-menu'),
                menu = $('#' + menu_attr);

            if (menu.length === 1) {
                $(this).toggleClass('is-active');
                menu.toggleClass('is-active');
            }
        });

        $('.panel-nav__title').on('click', function (e) {
            $(this).next('ul').toggleClass('open');
        });

        slidingMenu();

        scrollToFixed();

        if ($("#tabs").length > 0) {

            const options = {
                sectionClass: '.scrollspy', // Query selector to your sections
                menuActiveTarget: '.active', // Query selector to your elements that will be added `active` class
                offset: -200 // Menu item will active before scroll to a matched section 100px
            }

            // init:
            scrollSpy(document.getElementById('tabs'), options)

        }


        $('.aaa').scrollToFixed({
            limit: $('#footer').offset().top - $('.aaa').outerHeight(true)
        });


        //profile read more collapse
        var profile_more = $('#profile-more'),
            profile_more_btn = $('#profile-more-btn');
        profile_more.on("hide.bs.collapse", function () {
            profile_more_btn.text('Czytaj dalej');
        });
        profile_more.on("show.bs.collapse", function () {
            profile_more_btn.text('Zwiń')
        });

    });

    //formular dodawania ogloszenia
    announce_form()

    $(window).on({
        load: function () {

        },
        resize: function () {


        },
        scroll: function () {

        }
    });

}(jQuery));