<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="profile">
            <div class="profile__order">
                <div class="profile__head">
                <div class="container">
                    <div class="profile__head__inner">
                        <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="profile__profile">
                                <div class="img-box">
                                    <img src="assets/img/avatar_3.png" alt="">
                                </div>
                                <div class="content-box">
                                    <h1>Magda Markowska</h1>
                                    <div class="location"><span class="icon icon-pin"></span>Warszawa</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="profile__ranking">
                                <div class="num-box">4.5</div>
                                <div class="content-box">
                                    <div class="stars">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star1.0.svg">
                                        <img src="assets/img/star0.5.svg">
                                    </div>
                                    <span class="txt">na podstawie</span>
                                    <a href="#" class="opinions">15 opinii</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="profil__actions">
                                <a href="#" class="btn">Kontakt</a>
                                <a href="#" class="btn btn--white"><span class="icon icon-alarm-comments"></span><span class="txt">Wiadomość</span></a>
                            </div>
                        </div>
                    </div>
                    </div>    
                </div>
            </div>
                <div class="profile__tabs">
                <div class="container">
                    <div class="tabs" id="tabs">
                        <a href="#video" class="tab-4 tab active">
                            Wideo
                        </a>
                        <a href="#profile" class="tab-1 tab ">
                            Profil
                        </a>
                        <a href="#opinions" class="tab-2 tab">
                            Opinie
                            <div class="tab__counter">15</div>
                        </a>
                        <a href="#ogloszenia" class="tab-2 tab">
                            Ogłoszenia
                            <div class="tab__counter">2</div>
                        </a>
                    </div>
                </div>
            </div>
            </div>    
            <div class="content__wrapper" id="scroll_content">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-9">

                            <section class="profile scrollspy" id="video">
                                <header class="section-header">
                                    <h2 class="header__level__2">Wideo</h2>
                                </header>
                                <div class="content__container">
                                <iframe width="100%" height="315"
                                    src="https://www.youtube.com/embed/tgbNymZ7vqY"
                                    allowfullscreen="allowfullscreen"
                                    mozallowfullscreen="mozallowfullscreen" 
                                    msallowfullscreen="msallowfullscreen" 
                                    oallowfullscreen="oallowfullscreen" 
                                    webkitallowfullscreen="webkitallowfullscreen">
                                </iframe>
                                </div>    
                            </section>

                            <section class="profile scrollspy" id="profile">
                                <header class="section-header">
                                    <h2 class="header__level__2">Profil</h2>
                                </header>
                                <div class="content__container">
                                    Have you ever finally just gave in to the temptation and read your horoscope in the newspaper on Sunday morning? Sure, we all have. For most of us, it’s a curiosity, an amusement to see what they say our day will be like based on the sign of the zodiac that we were born under… <button id="profile-more-btn" class="btn-collapse" data-toggle="collapse" data-target="#profile-more">Czytaj dalej</button>
                                    <div id="profile-more" class="collapse">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </div>
                                </div>    
                            </section>

                            <section class="opinions scrollspy" id="opinions">
                                <header class="section-header">
                                    <h2 class="header__level__2">Opinie <span>(15)</span></h2>
                                    <button class="add-opinion"><img src="assets/img/icon_plus.svg" alt=""><span class="txt">Dodaj opinię</span></button>
                                </header>
                                <div class="opinions__list">
                                    
                                    <div class="testi-m testi-m--white d-block d-xl-none">
                                    <div class="testi-m__inner">
                                        <div class="testi-m__author">
                                            <div class="testi-m__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                        </div>
                                        <div class="testi-m__rating">
                                            <div class="testi-m__author-name">Tomek Nowak</div>
                                            <p class="testi-m__date">22 grudnia 2018</p>
                                            <div class="testi-m__stars stars">
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                            </div>
                                        </div>
                                    </div>
                                    <p class="testi-m__text">
                                        Have you ever finally just gave in to the temptation and read your 
                                        horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                        For most of us, will be like based on the sign of the zodiac that we were born under.
                                    </p>
                                </div>

                                    <div class="testi testi--white d-none d-xl-block">
                                    <div class="testi__inner">
                                        <div class="testi__author">
                                            <div class="testi__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                            <div class="testi__author-name">Tomek Nowak</div>
                                        </div>
                                        <div>
                                            <div class="testi__rating">
                                                <p class="testi__date">22 grudnia 2018</p>
                                                <div class="testi__stars stars">
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                </div>
                                            </div>
                                            <p class="testi__text">
                                                Have you ever finally just gave in to the temptation and read your 
                                                horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                                For most of us, will be like based on the sign of the zodiac that we were born under.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                    
                                    <div class="testi-m testi-m--white d-block d-xl-none">
                                    <div class="testi-m__inner">
                                        <div class="testi-m__author">
                                            <div class="testi-m__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                        </div>
                                        <div class="testi-m__rating">
                                            <div class="testi-m__author-name">Tomek Nowak</div>
                                            <p class="testi-m__date">22 grudnia 2018</p>
                                            <div class="testi-m__stars stars">
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                            </div>
                                        </div>
                                    </div>
                                    <p class="testi-m__text">
                                        Have you ever finally just gave in to the temptation and read your 
                                        horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                        For most of us, will be like based on the sign of the zodiac that we were born under.
                                    </p>
                                </div>

                                    <div class="testi testi--white d-none d-xl-block">
                                    <div class="testi__inner">
                                        <div class="testi__author">
                                            <div class="testi__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                            <div class="testi__author-name">Tomek Nowak</div>
                                        </div>
                                        <div>
                                            <div class="testi__rating">
                                                <p class="testi__date">22 grudnia 2018</p>
                                                <div class="testi__stars stars">
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                </div>
                                            </div>
                                            <p class="testi__text">
                                                Have you ever finally just gave in to the temptation and read your 
                                                horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                                For most of us, will be like based on the sign of the zodiac that we were born under.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                    
                                    <div class="testi-m testi-m--white d-block d-xl-none">
                                    <div class="testi-m__inner">
                                        <div class="testi-m__author">
                                            <div class="testi-m__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                        </div>
                                        <div class="testi-m__rating">
                                            <div class="testi-m__author-name">Tomek Nowak</div>
                                            <p class="testi-m__date">22 grudnia 2018</p>
                                            <div class="testi-m__stars stars">
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                                <img src="assets/img/star1.0.svg" />
                                            </div>
                                        </div>
                                    </div>
                                    <p class="testi-m__text">
                                        Have you ever finally just gave in to the temptation and read your 
                                        horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                        For most of us, will be like based on the sign of the zodiac that we were born under.
                                    </p>
                                </div>

                                    <div class="testi testi--white d-none d-xl-block">
                                    <div class="testi__inner">
                                        <div class="testi__author">
                                            <div class="testi__author-img">
                                                <img src="assets/img/testi_author.png" alt="Tomek Nowak" />
                                            </div>
                                            <div class="testi__author-name">Tomek Nowak</div>
                                        </div>
                                        <div>
                                            <div class="testi__rating">
                                                <p class="testi__date">22 grudnia 2018</p>
                                                <div class="testi__stars stars">
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                    <img src="assets/img/star1.0.svg" />
                                                </div>
                                            </div>
                                            <p class="testi__text">
                                                Have you ever finally just gave in to the temptation and read your 
                                                horoscope in the newspaper on Sunday morning? Sure, we all have. 
                                                For most of us, will be like based on the sign of the zodiac that we were born under.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                
                                </div>
                                <div class="text-right">
                                    <a href="#" class="opinions-next"><span class="txt">Pokaż kolejne</span><img src="assets/img/arrow_grey_2.svg" alt=""></a> 
                                </div>    
                            </section>  
                            
                             <section class="profile scrollspy" id="ogloszenia">
                                <header class="section-header">
                                    <h2 class="header__level__2">Ogłoszenia <span>(2)</span></h2>
                                </header>
                                <a href="#" class="advertisement advertisement--horiz">
                                    <div class="advert__head">
                                        <div class="advert__img">
                                            <img src="assets/img/img-advert.jpg" alt="">
                                        </div>
                                        <div class="advert__label">Muzyka</div>
                                    </div>
                                    <div class="advert__content">
                                        <div class="advert__content__row1">
                                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                            <div class="location">Warszawa</div>
                                            <div class="advert__details__price">
                                                <div class="price"><span>1100</span>zł</div>
                                            </div>
                                        </div>
                                        <div class="advert__content__row2">
                                            <div class="profil-rank">
                                                <div class="profil-rank__img">
                                                    <img src="assets/img/avatar.jpg" alt="">
                                                </div>
                                                <div class="profil-rank__content">
                                                    <div class="profil-rank__name">Adam Mucha</div>
                                                    <div class="profil-rank__rank">
                                                        <div class="stars half">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="advert__skills">
                                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                                <div class="skill">Rozwój osobisty</div>
                                                <div class="skill">Słuch</div>
                                                <div class="skill">Doskonalenie koordynacji</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="advertisement advertisement--horiz">
                                    <div class="advert__head">
                                        <div class="advert__img">
                                            <img src="assets/img/img-advert.jpg" alt="">
                                        </div>
                                        <div class="advert__label">Muzyka</div>
                                    </div>
                                    <div class="advert__content">
                                        <div class="advert__content__row1">
                                            <div class="advert__name">Kurs gry na gitarze - kurs podstawowy dla dzieci do 18 lat</div>
                                            <div class="location">Warszawa</div>
                                            <div class="advert__details__price">
                                                <div class="price"><span>1100</span>zł</div>
                                            </div>
                                        </div>
                                        <div class="advert__content__row2">
                                            <div class="profil-rank">
                                                <div class="profil-rank__img">
                                                    <img src="assets/img/avatar.jpg" alt="">
                                                </div>
                                                <div class="profil-rank__content">
                                                    <div class="profil-rank__name">Adam Mucha</div>
                                                    <div class="profil-rank__rank">
                                                        <div class="stars two-and-half">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="advert__skills">
                                                <div class="header">Umiejętności, które zdobędziesz:</div>
                                                <div class="skill">Rozwój osobisty</div>
                                                <div class="skill">Słuch</div>
                                                <div class="skill">Doskonalenie koordynacji</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                             </section>     
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="register-box-wrapper">
                                <div class="register-box">
                                <img src="assets/img/ilustracja-skillsy.svg" alt="">
                                <div class="desc">
                                    Założ darmowe konto i zacznij zarabiać na swoich Skillsach.
                                </div>
                                <a href="#" class="btn btn--red">Rejestracja</a>
                            </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
