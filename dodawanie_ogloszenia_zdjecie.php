<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="row">
                <div class="col-12 col-md-10 offset-md-1 col-lg-4 offset-lg-4">
                    <div class="content__container desktop__container">
                        <section class="crop-image">
                            <header class="section-header">
                                <h2 class="header__level__2">Skadruj swoje zdjęcie</h2>
                                <div class="image">
                                    <img src="assets/img/image.jpg" alt="" id="image">
                                </div>
                            </header>

                            <div class="center"><a href="#" class="btn submit">Gotowe</a></div>
                            
                        </section>
                    </div>
                </div>
            </div>

        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
