const announce_form = () => {
    
    $('#next__step').on('click', function(e){
        e.preventDefault()
        
        $('.register__step').removeClass('register__step--active')
        $('.register__step--step_2').addClass('register__step--active')
        
        $('.new__announce .step').removeClass('active-step')
        $('.new__announce .step-2').addClass('active active-step')
        
        $('html,body').stop().animate({scrollTop: 0}, 300)
    })
    
 }



 module.exports = {
     announce_form
 };