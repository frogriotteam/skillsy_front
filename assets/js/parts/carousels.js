const carousels = () => {
    
    var top_carousel = $('#categories__slider'),
        advert_carousel = $('#advert-slider'),
        opinions_carousel = $('#opinions-slider');

        opinions_carousel.slick({
          speed: 1500,
          infinite: true,
          arrows: false,
          slidesToShow: 1,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,

          responsive: [{
             breakpoint: 992,
              settings: {
                settings: "unslick"
              }
            }     
         ]
    })
    
        top_carousel.slick({
              speed: 10000,
              infinite: false,
              mobileFirst: true,
              arrows: false,
              slidesToShow: 1,
              autoplaySpeed: 0,
              cssEase: 'linear',
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,

              responsive: [{

                  breakpoint: 480,
                  settings: {
                    slidesToShow: 2,
                    infinite: true
                  }
                }, {
                 breakpoint: 768,
                  settings: {
                    slidesToShow: 3,
                    infinite: true
                  }
                }, {
                 breakpoint: 1024,
                  settings: {
                    slidesToShow: 4,
                    infinite: true
                  }
                }, {
                 breakpoint: 1300,
                  settings: {
                    slidesToShow: 5,
                    infinite: true
                  }
                }, {
                 breakpoint: 1600,
                  settings: {
                    slidesToShow: 6,
                    infinite: true
                  }
                }
                          
             ]
        })
    
        $('#advert-next').on('click', function(){
            advert_carousel.slick('slickNext');
        })
    
        $('#advert-prev').on('click', function(){
            advert_carousel.slick('slickPrev');
        })


        if ($(window).width() > 1400) {
          // On init
          advert_carousel.on('init', function(event, slick, direction){
                var slideWidth = advert_carousel.find('.slick-slide').width();
                advert_carousel.find('.slick-track').css('padding-left', slideWidth + 16);
                setTimeout(function(){
                  $('#advert-slider').slick('setPosition');
                },0)  
          });
        } 
    
        advert_carousel.slick({
              infinite: false,
              mobileFirst: true,
              arrows: false,
              slidesToShow: 1.1,
              autoplay: false,
              autoplaySpeed: 2000,

              responsive: [ {
                 breakpoint: 600,
                  settings: {
                    slidesToShow: 2.1,
                  }
                }, {
                 breakpoint: 800,
                  settings: {
                    slidesToShow: 3.1,
                  }
                }, {
                 breakpoint: 1200,
                  settings: {
                    slidesToShow: 3,
                  }
                },{
                 breakpoint: 1400,
                  settings: {
                    slidesToShow: 5
                  }
                }
                          
             ]
        })
    
 }



 module.exports = {
     carousels
 };