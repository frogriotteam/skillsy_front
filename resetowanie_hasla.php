<?php include 'include/head.php' ?>

        <?php include 'include/header.php' ?>

        <div class="row">
                <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                    <div class="content__container desktop__container">
                        <section class="reset-password">
                            <img src="assets/img/icon_lock.svg" alt="" class="icon">
                            <header class="section-header">
                                <h2 class="header__level__2">Przypomnij hasło</h2>
                                <div class="desc">
                                    Wpisz swój adres e-mail, który użyłeś do rejestracji konta. Wyślemy do Ciebie wiadomość e-mail z Twoją nazwą użytkownika oraz z łączem umożliwiającym zresetowanie hasła.
                                </div>
                            </header>
                            <form id="form" class="form">
                                <input type="email" placeholder="Twój e-mail użyty przy rejestracji konta" name="email">
                                <button type="submit" class="btn btn-wide submit">Przypomnij hasło</button>
                                <a href="#" class="forgot-pass">Powrót do strony logowania</a>
                            </form>
                        </section>
                    </div>
                </div>
            </div>

        
        <?php include 'include/footer.php' ?>

        <?php include 'include/javascript.php' ?>

        
    </body>
</html>
